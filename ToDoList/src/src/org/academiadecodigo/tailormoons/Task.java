package src.org.academiadecodigo.tailormoons;

public class Task implements Comparable<Task>   {

    private String name;
    private TaskImportance importance;
    private int priority;

    public Task(String name, TaskImportance importance, int priority) {
        this.name = name;
        this.importance = importance;
        this.priority = priority;
    }


    @Override
    public int compareTo(Task o) {

        if(importance.getValue() > o.getImportance().getValue()) {
            return 1;
        } else if (importance.getValue() < o.getImportance().getValue()) {
            return -1;
        }

        //if importance is the same, sort by priority
        if(getPriority() > o.getPriority()) {
            return 1;
        } else if (getPriority() < o.getPriority()) {
            return -1;
        }

        return 0;

        /*//HIGH
        if(importance == TaskImportance.HIGH) {
            if(o.getImportance() == TaskImportance.HIGH) {
                return 0;
            } else {
                return -1;
            }
        }

        //MEDIUM
        if(importance == TaskImportance.MEDIUM) {
            if(o.getImportance() == TaskImportance.MEDIUM) {
                return 0;
            } else if(o.getImportance() == TaskImportance.HIGH){
                return 1;
            } else { //LOW
                return -1;
            }
        }

        //LOW
        if(importance == TaskImportance.LOW) {
            if(o.getImportance() == TaskImportance.LOW) {
                return 0;
            } else {
                return 1;
            }
        }

        return 3;*/
    }

    public TaskImportance getImportance() {
        return importance;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getPriority() {
        return priority;
    }
}
